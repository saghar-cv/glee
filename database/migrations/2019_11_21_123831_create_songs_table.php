<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSongsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('songs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('artist');
            $table->string('name');
            $table->json('genres')->nullable();;
            $table->unsignedBigInteger('playlist_id');
            $table->unsignedBigInteger('artist_id');
            $table->string('cover_path')->nullable();
            $table->string('music_path')->nullable();
            $table->tinyInteger('status');
            $table->tinyInteger('trend')->default(0);
            $table->dateTime('date')->nullable();
            $table->timestamps();

            $table->foreign('playlist_id')->references('id')->on('playlists');
            $table->foreign('artist_id')->references('id')->on('artists');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('songs', function (Blueprint $table) {
            $table->dropForeign('songs_playlist_id_foreign');
            $table->dropForeign('songs_artist_id_foreign');
        });
        Schema::dropIfExists('songs');
    }
}
