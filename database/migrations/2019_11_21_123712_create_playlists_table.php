<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlaylistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('playlists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->dateTime('date')->nullable();
            $table->unsignedBigInteger('host_id')->nullable();
            $table->timestamps();

            $table->foreign('host_id')->references('id')->on('hosts');
        });

    }

    /**
     * Reverse the migrations.

     * @return void
     */
    public function down()
    {
        Schema::table('playlists', function (Blueprint $table) {
            $table->dropForeign('playlists_host_id_foreign');
        });
        Schema::dropIfExists('playlists');
    }
}
