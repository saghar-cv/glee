<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Admin
 * @package App
 *
 * @property integer $id
 * @property string $artist
 * @property string $name
 * @property array $genres
 * @property integer $artist_id
 * @property integer $playlist_id
 * @property string $cover_path
 * @property string $music_path
 * @property integer $status
 * @property integer $trend
 * @property \DateTime $date
 */
class Song extends Model
{
    const STATUS_PENDING = 0;
    const STATUS_CONFIRMED = 1;

    const NOT_TREND = 0;
    const TREND = 1;

    protected $fillable = [
        'artist',
        'name',
        'genres',
        'playlist_id',
        'artist_id',
        'cover_path',
        'music_path',
        'status',
        'date',
    ];

    /**
     * Get playlist
     *
     * @return BelongsTo
     */
    public function getPlaylist()
    {
        return $this->belongsTo('App\Playlist', 'playlist_id');
    }

    public function getArtist()
    {
        return $this->belongsTo('App\Artist', 'artist_id');
    }
}
