<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Admin
 * @package Playlist
 *
 * @property integer $id
 * @property string $name
 * @property Date $date
 * @property integer $host_id
 */
class Playlist extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'date', 'host_id'
    ];


    /**
     * Get playlist host
     *
     * @return BelongsTo
     */
    public function getHost()
    {
        return $this->belongsTo('App\Host', 'host_id');
    }
}
