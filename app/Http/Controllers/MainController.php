<?php

namespace App\Http\Controllers;

use App\Artist;
use App\Song;
use App\Playlist;
use App\Host;
use Carbon\Carbon;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Storage;

class MainController extends Controller
{

    /**
     * Get Current week's musics from spotifynewmusic.com
     * @throws GuzzleException
     */
    public function spotifyNewMusicMainPage()
    {
        $url = 'http://spotifynewmusic.com';
        $dom = htmlBodyContent($url);
        $nodes = getElementByClassName($dom, 'album');
        if (!($host = Host::getHostByAddress($url))) {
            $host = new Host();
            $host->address = $url;
            $host->save();
        }

        $now = Carbon::now();
        $weekStartDate = $now->startOfWeek()->format('Y-m-d');

        $playlist = new PlayList();
        $playlist->name = $weekStartDate;
        $playlist->date = $weekStartDate;
        $playlist->host_id = $host->id;
        $playlist->save();

        $genres = [];
        foreach ($nodes as $node) {
            $song = new Song();
            $song->playlist_id = $playlist->id;
            $aTags = $node->getElementsByTagName('a');
            foreach ($aTags as $aTag) {
                if (($aTag->getElementsByTagName('strong')->length) != 0) {
                    $bandName = "";
                    foreach ($aTag->getElementsByTagName('strong') as $value) {
                        $genres = [];
                        $bandName = $value->nodeValue;
                        $song->artist = substr_replace($bandName, '', 0, 1);

                        if ($artist = Artist::where('name', $song->artist)->first()) {
                            $song->artist_id = $artist->id;
                        } else {
                            $artist = new Artist();
                            $artist->name = $song->artist;
                            $artist->save();
                            $song->artist_id = $artist->id;
                        }
                    }
                    $song->name = substr_replace($aTag->nodeValue, '', 0, strlen($bandName) + 3);
                }
                if (strpos($aTag->getAttribute('href'), 'spotifygenres.php') !== false) {
                    array_push($genres, $aTag->nodeValue);
                }
                $song->genres = json_encode($genres);
            }

            $images = $aTags[0]->getElementsByTagName('img');
            foreach ($images as $image) {
                if ($image->getAttribute('src') != 'playnew.jpg') {
                    $imgUrl = $url . $image->getAttribute('src');
                    $contents = file_get_contents($imgUrl);
                    $name = substr($imgUrl, strrpos($imgUrl, '/') + 1);
                    Storage::put('public/cover/'. $name, $contents);
                    $song->cover_path = '/storage/app/public/cover/' . $name;
                }
            }
            $song->status = Song::STATUS_PENDING;
            $song->date = $playlist->date;

            $download = download($song->artist, $song->name);
            if (is_array($download))
                $song->music_path = '/storage/app/public/music' . $download[0];

            $song->save();

        }
    }

    /**
     * Get archived music form spotifynewmusic.com
     * @throws GuzzleException
     */
    public function spotifyNewMusicArchive()
    {
        $url = 'http://spotifynewmusic.com';
        $dom = htmlBodyContent($url);
        $div = getElementByClassName($dom, 'column2')[0];
        $select = $div->getElementsByTagName('select');
        foreach ($select[0]->childNodes as $childNode) {
            $date = $childNode->nodeValue;
            if ($date != 'This Week') {
                $dom = htmlBodyContent($url . '/index.php?sort=da&date=' . $date);
                $nodes = getElementByClassName($dom, 'album');
                if (!($host = Host::getHostByAddress($url))) {
                    $host = new Host();
                    $host->address = $url;
                    $host->save();
                }

                $playlist = new PlayList();
                $playlist->name = $date;
                $dateArr = explode('-', $date);
                $playlist->date = Carbon::create($dateArr[0], $dateArr[1], $dateArr[2]);
                $playlist->host_id = $host->id;
                $playlist->save();

                $genres = [];
                foreach ($nodes as $node) {
                    $song = new Song();
                    $song->playlist_id = $playlist->id;
                    $aTags = $node->getElementsByTagName('a');
                    foreach ($aTags as $aTag) {
                        if (($aTag->getElementsByTagName('strong')->length) != 0) {
                            $bandName = "";
                            foreach ($aTag->getElementsByTagName('strong') as $value) {
                                $genres = [];
                                $bandName = $value->nodeValue;
                                $song->artist = substr_replace($bandName, '', 0, 1);
                                if ($artist = Artist::where('name', $song->artist)->first()) {
                                    $song->artist_id = $artist->id;
                                } else {
                                    $artist = new Artist();
                                    $artist->name = $song->artist;
                                    $artist->save();
                                    $song->artist_id = $artist->id;
                                }
                            }
                            $song->name = utf8_encode(substr_replace($aTag->nodeValue, '', 0, strlen($bandName) + 3));
                        }
                        if (strpos($aTag->getAttribute('href'), 'spotifygenres.php') !== false) {
                            array_push($genres, $aTag->nodeValue);
                        }
                        $song->genres = json_encode($genres);
                    }

                    $images = $aTags[0]->getElementsByTagName('img');
                    foreach ($images as $image) {
                        if ($image->getAttribute('src') != 'playnew.jpg') {
                            $imgUrl = $url . $image->getAttribute('src');
                            $contents = file_get_contents($imgUrl);
                            $name = substr($imgUrl, strrpos($imgUrl, '/') + 1);
                            Storage::put('public/cover/' . $name, $contents);
                            $song->cover_path = '/storage/app/public/cover/' . $name;
                        }
                    }
                    $song->status = Song::STATUS_PENDING;
                    $song->date = $playlist->date;

                    $download = download($song->artist, $song->name);
                    if (is_array($download))
                        $song->music_path = '/storage/app/public/music' . $download[0];

                    $song->save();
                }
            }
        }
    }

}
