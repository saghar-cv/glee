<?php

/**
 * Get body of html content of a web page
 *
 * @param $url
 * @return DOMDocument
 * @throws \GuzzleHttp\Exception\GuzzleException
 */
function htmlBodyContent($url)
{
    $guzzleClient = new GuzzleHttp\Client(array(
        'timeout' => 60,
    ));
    $guzzleResponse = $guzzleClient->request('GET', $url);
    $dom = new DOMDocument;
    libxml_use_internal_errors(true);
    $dom->loadHTML($guzzleResponse->getBody()->getContents());
    libxml_clear_errors();

    return $dom;
}

/**
 * Get html element by classname
 *
 * @param $dom
 * @param $classname
 * @return DOMNodeList|false
 */
function getElementByClassName($dom, $classname)
{
    $finder = new DOMXPath($dom);
    $nodes = $finder->query("//*[contains(@class, '$classname')]");
    return $nodes;
}

/**
 * Download mp3 music from youtube by artist and song name and returns song's file name
 *
 * @param $artist
 * @param $song_name
 * @return mixed
 */
function download($artist, $song_name)
{
    exec("youtube-dl -o \"/storage/app/public/music/%(title)s.%(ext)s\" \"ytsearch1:" . $artist . "-" . $song_name . "\" -x --audio-format \"mp3\"");
    exec("youtube-dl --get-filename \"ytsearch1:" . $artist . "-" . $song_name . "\" -x --audio-format \"mp3\"", $output);
    return $output;
}
